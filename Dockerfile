FROM registry:5000/mule38base:3.8.5

MAINTAINER James Jiang <jamjiang@deloitte.com.au>

ENV MULE_APP helloworld


# Uncomment for fine tuning, must be consistent with Kubernetes 
# Or else overwrite from Openshift Pod Environment Variables

# ENV MULE_MEMORY_MIN_HEAP 128
# ENV MULE_MEMORY_MAX_HEAP 256
# ENV MULE_STARTUP_TIMEOUT 600

COPY target/${MULE_APP}-*.zip $MULE_HOME/apps/${MULE_APP}.zip


EXPOSE 8888

CMD ["/start.sh"]
