Hello World

This is a simple Mule application created for the demonstration "Mule in a box" at Mule Summit 2018

# Compile

    mvn clean package
    
# Run Unit Tests

    mvn clean test
 

# Build Docker Image

    ./> docker build -t <registry>/helloworld:<tag> .

